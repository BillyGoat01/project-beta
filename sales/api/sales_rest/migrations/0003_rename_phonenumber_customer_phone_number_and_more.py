# Generated by Django 4.0.3 on 2022-09-13 05:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_status_sellcar_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customer',
            old_name='phoneNumber',
            new_name='phone_number',
        ),
        migrations.AddField(
            model_name='automobilevo',
            name='import_href',
            field=models.CharField(default=1, max_length=200, unique=True),
            preserve_default=False,
        ),
    ]
