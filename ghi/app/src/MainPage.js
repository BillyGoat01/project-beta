function MainPage() {
  const containerStyle = {
    backgroundImage: `url(https://i.dailymail.co.uk/i/newpix/2018/03/26/15/4A92DD3F00000578-0-image-a-62_1522076120466.jpg)`,
    backgroundSize: 'cover',
    backgroundPosition: 'bottom left',
    width: '1000px',
    height: '450px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    padding: '0 20px'  // Some padding to ensure content doesn't stick to the edges
  };

  return (
    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '10px'}}>
      <div style={containerStyle}>
        <h1 className="display-5 fw-bold"  style={{color: 'white'}}>CarCar</h1>
        <div className="col-lg-6">
          <p className="lead mb-4" style={{color: 'white'}}>
            The premiere solution for automobile dealership
            management!
          </p>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
