import React from "react";

class AutomobileList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            autos: [],
        };
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            this.setState({autos: data.autos})
            console.log(data)

        }
    }

    render() {
        return (
            <>
                <div>
                <h1>Vehicle Inventory</h1>
                <table className="table table-striped" style={{marginTop: "20px"}}>
                    <thead>
                        <tr>
                           
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Manufacture</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.autos.map(auto => {
                            return (
                                <tr key={auto.href}>
                            
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                </div>
            </>
        )
    }
}

export default AutomobileList;