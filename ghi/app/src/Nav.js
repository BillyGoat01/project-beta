import { NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

function Navigation() {


  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse " id="navbarNavDropdown">
          <ul className="navbar-nav  w-100 d-flex justify-content-around">
            <li className="nav-item dropdown">
              <a className="nav-link" href="#" id="navbarDropdownMenuLink1" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </a>
              <ul className="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMenuLink1">
                <li><NavLink to='models' className="dropdown-item text-white bg-dark" href="#">Models</NavLink></li> {/* Updated classes for dropdown items */}
                <li><NavLink to='model/form' className="dropdown-item text-white bg-dark">Create Models</NavLink></li>
                <li><NavLink to='automobiles/' className="dropdown-item text-white bg-dark" href="#">Automobiles</NavLink></li>
                <li><NavLink to='automobiles/new/' className="dropdown-item text-white bg-dark" href="#">Create a Automobile</NavLink></li>
                <li><NavLink to='manufacturers/' className="dropdown-item text-white bg-dark" href="#">Make</NavLink></li>
                <li><NavLink to='manufacturers/new/' className="dropdown-item text-white bg-dark" href="#">Create a Make</NavLink></li>
                <li><NavLink to='cars/sold/history' className="dropdown-item text-white bg-dark" href="#">Veihicle Sold History</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link " href="#" id="navbarDropdownMenuLink2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMenuLink2">
                <li><NavLink to='cars/sold' className="dropdown-item text-white bg-dark" href="#">Cars Sold</NavLink></li> {/* Updated classes for dropdown items */}
                <li><NavLink to='customer/form' className="dropdown-item text-white bg-dark" href="#">Create A Customer</NavLink></li>
                <li><NavLink to='salesperson/form' className="dropdown-item text-white bg-dark" href="#">Create Employ</NavLink></li>
                <li><NavLink to='carsale/form' className="dropdown-item text-white bg-dark" href="#">Sell a Car</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link " href="#" id="navbarDropdownMenuLink3" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </a>
              <ul className="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMenuLink3">
                <li><NavLink to='appointments/' className="dropdown-item text-white bg-dark" href="#">Appointments</NavLink></li> {/* Updated classes for dropdown items */}
                <li><NavLink to='appointments/new/' className="dropdown-item text-white bg-dark" href="#">Create Appointments</NavLink></li>
                <li><NavLink to='technician/new/' className="dropdown-item text-white bg-dark" href="#">Create Technician</NavLink></li>
                <li><NavLink to='history/' className="dropdown-item text-white bg-dark" href="#">Service History</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    // <nav className="navbar navbar-expand-lg navbar-dark bg-success">
    //   <div className="container-fluid">
    //     <NavLink className="navbar-brand" to="/">CarCar</NavLink>
    //     <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    //       <span className="navbar-toggler-icon"></span>
    //     </button>
    //     <div className="collapse navbar-collapse" id="navbarSupportedContent">
    //       <ul className="navbar-nav me-auto mb-2 mb-lg-0">
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="/">Home</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="cars/sold">Sold Cars</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="customer/form">Customer Form</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="salesperson/form">Sales Person Form</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="carsale/form">Car Sale Form</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="models">Models</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="model/form">Model Form</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="cars/sold/history">Sales People History</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="appointments/">Appointment List</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="appointments/new/">New Appointment</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="technician/new/">Enter a Technician</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="automobiles/">Automobiles</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="manufacturers/">Manufacturers</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="manufacturers/new/">Create a Manufacturers</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="automobiles/new/">Create a Automobile</NavLink>
    //         </li>
    //         <li className="nav-item">
    //           <NavLink className="nav-link" to="history/">Service History</NavLink>
    //         </li>
    //       </ul>
    //     </div>
    //   </div>
    // </nav>
  )
}

export default Navigation;
