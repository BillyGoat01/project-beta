import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Navigation from './Nav';
import SalesList from './Sales/SalesList';
import CustomerForm from './Sales/CustomerForm';
import SalesPersonForm from './Sales/SalesPersonForm';
import SalesForm from './Sales/SalesForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import SalesHistory from './Sales/SalesPersonHistory';
import AppointmentList from './service/AppointmentList';
import AppointmentForm from './service/AppointmentForm';
import { NewTechnicianForm } from './service/NewTechnicianForm';
import AutomobileList from './inventory/AutomobileList';
import AutomobileForm from './inventory/AutomobileForm';
import ManufacturersList from './inventory/ManufacturerList';
import ManufacturerForm from './inventory/ManufacturerForm';
import ServiceHistory from './service/ServiceHistory';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <BrowserRouter>
      <Navigation />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="cars/sold" element={<SalesList />} />
          <Route path="customer/form" element={<CustomerForm />} />
          <Route path="salesperson/form" element={<SalesPersonForm />} />
          <Route path="carsale/form" element={<SalesForm />} />
          <Route path="models" element={<ModelList />} />
          <Route path="model/form" element={<ModelForm />} />
          <Route path="cars/sold/history" element={<SalesHistory />} />
          <Route path="appointments/" element={<AppointmentList />} />
          <Route path="appointments/new/" element={<AppointmentForm />} />
          <Route path='technician/new/' element={<NewTechnicianForm />} />
          <Route path='automobiles/' element={<AutomobileList />} />
          <Route path='automobiles/new/' element={<AutomobileForm />} />
          <Route path='manufacturers/' element={<ManufacturersList />} />
          <Route path='manufacturers/new/' element={<ManufacturerForm />} />
          <Route path='history/' element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
