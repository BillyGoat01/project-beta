import React from 'react';


class SalesForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cars: [],
            salesPersons: [],
            customers: [],
            salesVins: [],
            price: '',
        }
        this.handleCarChange = this.handleCarChange.bind(this);
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this); 
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.sales_person = data.salesPerson
        delete data.salesPerson
        delete data.cars
        delete data.salesPersons
        delete data.customers;
        delete data.salesVins

        const url = "http://localhost:8090/api/cars/sold/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)
        console.log(response)
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale)

            const clear = {
                car: '',
                sales_person: '',
                salesPerson: '',
                price: '',
            }
            this.setState(clear)
            window.location.reload()
        }
    }
     async componentDidMount() {
        const salesURL = "http://localhost:8090/api/cars/sold/";
        const salesResponse = await fetch(salesURL);
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            let vinList = []
            for (let sales of salesData.sales){
                vinList.push(sales.car.vin)
            }
            this.setState({salesVins: vinList})
        }
        const carUrl = "http://localhost:8090/api/cars/";
        const carResponse = await fetch(carUrl);
        if (carResponse.ok) {
            const carData = await carResponse.json();
            let carList = []
            console.log(carList)
             this.setState({cars: carData.cars})
        }
        const salesPersonUrl = "http://localhost:8090/api/sales/person/";
        const salesPersonresponse = await fetch(salesPersonUrl);
        if (salesPersonresponse.ok) {
            const salesPersondata = await salesPersonresponse.json()
            this.setState({salesPersons: salesPersondata.sales_people})
        }
        const customerUrl = "http://localhost:8090/api/sales/customers/";
        const customerResponse = await fetch(customerUrl);
        if (customerResponse.ok) {
            const data = await customerResponse.json()
            this.setState({ customers: data.customers })
        }
    }
    handleCarChange(event) {
        const value = event.target.value
        this.setState({ car: value })
    }
    handleSalesPersonChange(event) {
        const value = event.target.value
        this.setState({ salesPerson: value })
    }
    handleCustomerChange(event) {
        const value = event.target.value
        this.setState({ customer: value })
    }
    handlePriceChange(event) {
        const value = event.target.value
        this.setState({ price: value })
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Vehicle Sale</h1>
                        <form onSubmit={this.handleSubmit} id="create-customer-form">
                            <div className="mb-3">
                                <select  value={this.state.car} onChange={this.handleCarChange}  
                                    required name="car" id="car" className="form-select">
                                    <option value="car">Cars</option>
                                    {this.state.cars.map(vehicle => {
                                        if (!(this.state.salesVins.includes(vehicle.vin))){
                                        return (
                                            <option key={vehicle.vin} value={vehicle.vin}>
                                                {vehicle.vin}
                                            </option>
                                        );
                                        }
                                        })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.customer} onChange={this.handleCustomerChange}
                                    required name="customer" id="customer" className="form-select">
                                    <option value="customer">Customers</option>
                                    {this.state.customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>
                                                {customer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.salesPerson} onChange={this.handleSalesPersonChange}
                                    required name="sp" id="sp" className="form-select">
                                    <option value="sp">Sales Person</option>
                                    {this.state.salesPersons.map(seller => {
                                        return (
                                            <option key={seller.employee_num} value={seller.employee_num}>
                                                {seller.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.price} onChange={this.handlePriceChange}
                                 placeholder="price" required type="number" name="price" id="price" className="form-control" />
                                <label htmlFor="price">Price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default SalesForm