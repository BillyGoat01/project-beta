import React from 'react';

class SalesHistory extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = {
            sales: [],
            salesMen: [],
            sales_person: ''
        }
        this.handleSalesChange = this.handleSalesChange.bind(this)
    }
    async handleSalesChange(event){
        const value = event.target.value
        this.setState({sales_person: value})
    }
    async componentDidMount() {
        const carsUrl = "http://localhost:8090/api/cars/sold/"
        const carsResponse = await fetch(carsUrl);
        if (carsResponse.ok) {
            const carData = await carsResponse.json();
            console.log(carData)
            // let list = []
            // for (let car of carData.sales){
            //     if (this.state.sales_person === car.sales_person.employee_num){
            //         list.push(car)
            //     }
            // }
            this.setState({sales: carData.sales})
        }
        const salesManUrl = "http://localhost:8090/api/sales/person/"
        const response = await fetch(salesManUrl);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ salesMen: data.sales_people })
        }
    }
    render() {
        return (
            <div className="container">
                <div className="mb-3" style={{marginTop: '10px'}}>
                    <select value={this.state.sales_person} onChange={this.handleSalesChange}
                        required name="sales_person" id="sales_person" className="form-select">
                        <option value="">Sales People</option>
                        {this.state.salesMen.map(salesPerson => {
                            return (
                                <option key={salesPerson.employee_num} value={salesPerson.employee_num}>
                                    {salesPerson.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>Vin</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales.map(sale => {
                            if (sale.sales_person.employee_num === Number(this.state.sales_person)){
                            return (
                                <tr key={sale.id}>
                                    <td>{sale.sales_person.name}</td>
                                    <td>{sale.customer.name}</td>
                                    <td>{sale.car.vin}</td>
                                    <td>${sale.price}</td>
                                </tr>
                            );
                            }
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default SalesHistory